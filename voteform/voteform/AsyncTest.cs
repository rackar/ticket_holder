﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace voteform
{
    public partial class AsyncTest : Form
    {
        public AsyncTest()
        {
            InitializeComponent();
        }
        //【2】定义委托类型
        public delegate int MyDelegateClassName(int t,int ms);

        //【3】声明委托变量
        MyDelegateClassName mydelgt = null;

        //【1】定义待调用常用方法
        public int MyNormalMethed(int t,int ms)
        {
            //系统等待ms毫秒
            System.Threading.Thread.Sleep(ms);
            return t * t;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //【4】将方法(名)传入委托变量
             mydelgt = MyNormalMethed;
            //【5】定义异步接口变量,开启执行委托线程
            IAsyncResult result = mydelgt.BeginInvoke(10,500, null, null);
           label1.Text= "100";
            //【6】检测委托执行完毕事件，继续执行后面的语句。
            label2.Text = mydelgt.EndInvoke(result).ToString();
            MessageBox.Show("异步执行结束");
        }

        //【7】测试回调
        private void button2_Click(object sender, EventArgs e)
        {
            mydelgt = MyNormalMethed;
            for (int i = 0; i < 8; i++)
            {
                //除方法所需参数外，倒数第二个参数为回调函数名，最后一个参数为任意object回传（通过IA传入实例的result.AsyncState获取）
                IAsyncResult result = mydelgt.BeginInvoke(10+i, 500*i, MyCallBack, i);

            }
        }

        
        //【7】测试，定义异步回调函数
        private void MyCallBack(IAsyncResult result)
        {
            int res = mydelgt.EndInvoke(result);
            Console.WriteLine("第{0}次执行为{1}", ((int)result.AsyncState+1).ToString(), res.ToString());
            
        }
    }
}
