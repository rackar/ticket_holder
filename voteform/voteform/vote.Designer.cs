﻿namespace voteform
{
    partial class vote
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_adduser = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel_main = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_subtractinguser = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.num_votemax = new System.Windows.Forms.NumericUpDown();
            this.lbr_votecount = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_cancelvote = new System.Windows.Forms.Button();
            this.btn_vote = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_votemax)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_adduser
            // 
            this.btn_adduser.Location = new System.Drawing.Point(1004, 6);
            this.btn_adduser.Name = "btn_adduser";
            this.btn_adduser.Size = new System.Drawing.Size(164, 63);
            this.btn_adduser.TabIndex = 0;
            this.btn_adduser.Text = "+ 增加候选人";
            this.btn_adduser.UseVisualStyleBackColor = true;
            this.btn_adduser.Click += new System.EventHandler(this.btn_adduser_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel_main, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.16456F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.83544F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1758, 879);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // panel_main
            // 
            this.panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_main.Location = new System.Drawing.Point(3, 110);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(1752, 703);
            this.panel_main.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Controls.Add(this.btn_subtractinguser);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.num_votemax);
            this.panel1.Controls.Add(this.lbr_votecount);
            this.panel1.Controls.Add(this.btn_adduser);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1752, 101);
            this.panel1.TabIndex = 2;
            // 
            // btn_subtractinguser
            // 
            this.btn_subtractinguser.Location = new System.Drawing.Point(1227, 6);
            this.btn_subtractinguser.Name = "btn_subtractinguser";
            this.btn_subtractinguser.Size = new System.Drawing.Size(164, 63);
            this.btn_subtractinguser.TabIndex = 4;
            this.btn_subtractinguser.Text = "- 删除候选人";
            this.btn_subtractinguser.UseVisualStyleBackColor = true;
            this.btn_subtractinguser.Click += new System.EventHandler(this.btn_subtractinguser_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15F);
            this.label1.Location = new System.Drawing.Point(444, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "每张选票最多可选";
            // 
            // num_votemax
            // 
            this.num_votemax.Font = new System.Drawing.Font("宋体", 15F);
            this.num_votemax.Location = new System.Drawing.Point(705, 15);
            this.num_votemax.Margin = new System.Windows.Forms.Padding(4);
            this.num_votemax.Name = "num_votemax";
            this.num_votemax.Size = new System.Drawing.Size(110, 42);
            this.num_votemax.TabIndex = 0;
            this.num_votemax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbr_votecount
            // 
            this.lbr_votecount.AutoSize = true;
            this.lbr_votecount.Font = new System.Drawing.Font("宋体", 15F);
            this.lbr_votecount.Location = new System.Drawing.Point(39, 18);
            this.lbr_votecount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbr_votecount.Name = "lbr_votecount";
            this.lbr_votecount.Size = new System.Drawing.Size(0, 30);
            this.lbr_votecount.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15F);
            this.label2.Location = new System.Drawing.Point(796, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 30);
            this.label2.TabIndex = 3;
            this.label2.Text = " 项";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_cancelvote);
            this.panel2.Controls.Add(this.btn_vote);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 819);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1752, 57);
            this.panel2.TabIndex = 3;
            // 
            // btn_cancelvote
            // 
            this.btn_cancelvote.Location = new System.Drawing.Point(801, 3);
            this.btn_cancelvote.Name = "btn_cancelvote";
            this.btn_cancelvote.Size = new System.Drawing.Size(111, 50);
            this.btn_cancelvote.TabIndex = 1;
            this.btn_cancelvote.Text = "撤销错票";
            this.btn_cancelvote.UseVisualStyleBackColor = true;
            this.btn_cancelvote.Click += new System.EventHandler(this.btn_cancelvote_Click);
            // 
            // btn_vote
            // 
            this.btn_vote.Location = new System.Drawing.Point(536, 3);
            this.btn_vote.Name = "btn_vote";
            this.btn_vote.Size = new System.Drawing.Size(123, 50);
            this.btn_vote.TabIndex = 0;
            this.btn_vote.Text = "投票";
            this.btn_vote.UseVisualStyleBackColor = true;
            this.btn_vote.Click += new System.EventHandler(this.btn_vote_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1758, 879);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num_votemax)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_adduser;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel_main;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_vote;
        private System.Windows.Forms.Button btn_cancelvote;
        private System.Windows.Forms.Label lbr_votecount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown num_votemax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_subtractinguser;
    }
}

