﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace voteform
{
    public partial class vote : Form
    {
        //单个候选人实例
        votemember vm;
        //所有候选人集合
        List<votemember> vmlist = new List<votemember>();


        public vote()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// 增加候选人实例和控件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_adduser_Click(object sender, EventArgs e)
        {
            //实例化单个候选人
            vm = new votemember();
            //将本窗体信息当做属性传入
            vm.myForm = this;
            //已存在几个候选人，作为新候选人的索引
            vm.index = vmlist.Count();
            //动态将控件实例化
            vm.addControlView();
            panel_main.Controls.Add(vm.myPanel);

            //本次新增候选人加入候选人集合
            vmlist.Add(vm);
          

        }
        /// <summary>
        /// 删掉最后新增候选人
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_subtractinguser_Click(object sender, EventArgs e)
        {
            if (vmlist.Count() - 1>=0) { 
            vm =vmlist[vmlist.Count() - 1];
            vm.myPanel.Dispose();
            vmlist.RemoveAt(vmlist.Count() - 1);
            }
        }

        /// <summary>
        /// 进行投票
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btn_vote_Click(object sender, EventArgs e)
        {

            #region 进行是否没有进行任何勾选的判断
            bool isVoteNull = false;
            foreach (votemember v in vmlist)
            {
                if (v.myCheckPic.Checked)
                {
                    isVoteNull = false;
                    break;
                }
                else
                    isVoteNull = true;
            }
            if (isVoteNull)
            {
                MessageBox.Show("未进行勾选");
                return;
            }
            #endregion



            #region 进行本张选票所选的项目数超出上限的判断
            int checkedCount = 0;
            foreach(votemember v in vmlist)
            {
                if (v.myCheckPic.Checked)
                {
                    checkedCount++;
                }
                
            }
            if (checkedCount > num_votemax.Value)
            {
                MessageBox.Show("选票所选的项目数超出上限");
                return;
            }
            
            #endregion 



            foreach (votemember v in vmlist)
            {
                if (v.myCheckPic.Checked)
                {
                    v.Num_result = 1;
                    v.myCheckPic.Checked = false;
                    v.myCheckPic.Image = global::voteform.Properties.Resources._2;
                }

                //投票结果保存入集合
                v.vote_count.Add(v.Num_result);
                
                //重置值
                v.Num_result = 0;

                //票数累加
                int vCountAll = 0;
                foreach (int i in v.vote_count)
                    vCountAll += i;

                v.myResult.Text = vCountAll.ToString();
                

            }
            refresh_VoteCount();
        }
        /// <summary>
        /// 取消上一张选票
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_cancelvote_Click(object sender, EventArgs e)
        {
            if (vmlist[0].vote_count.Count() == 0)
            { MessageBox.Show("已全部删除");
                return;
            }
            foreach (votemember v in vmlist)
            {
                if (v.myCheckPic.Checked)
                {

                    v.myCheckPic.Checked = false;
                    v.myCheckPic.Image = global::voteform.Properties.Resources._2;
                }
                if (v.vote_count.Count() >= 1)
                    v.vote_count.RemoveAt(v.vote_count.Count() - 1);
                


                int vCountAll = 0;
                foreach (int i in v.vote_count)
                    vCountAll += i;

                v.myResult.Text = vCountAll.ToString();


            }
            
            
            refresh_VoteCount();

        }

        /// <summary>
        /// 刷新界面上已投票数信息
        /// </summary>
        void refresh_VoteCount()
        {
            if (vm == null)
                return; 
            if(vmlist!=null)
            lbr_votecount.Text = "已记录"+vmlist[0].vote_count_num+"张选票";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }


    /// <summary>
    /// 定义一个投票对象，包含动态生成他的控件
    /// </summary>
    public class votemember
    {
        /// <summary>
        /// 候选人控件
        /// </summary>
        public Panel myPanel;
        public TextBox myName;
      //  public CheckBox myCheck;
        public CheckedPicbox myCheckPic;
        public Label myResult;
        public Form myForm;
        /// <summary>
        /// 第几位候选人
        /// </summary>
        public int index = 0;

        /// <summary>
        /// 候选人单次记票结果：0或1
        /// </summary>
        public int Num_result = 0;

        /// <summary>
        /// 界面上每行生产的控件数
        /// </summary>
        public int Num_eachline = 6;
        /// <summary>
        /// 本候选人每次记票结果
        /// </summary>
        public List<int> vote_count=new List<int>();

        public int vote_count_num {
            get
            {
                return vote_count.Count;
            }
                
            set { } }

        /// <summary>
        /// 调用时选中名字控件的文字
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SelectName(object sender, EventArgs e)
        {
            myName.SelectAll();
        }

        void Checkpic(object sender, EventArgs e)
        {
            if (myCheckPic.Checked)
            { myCheckPic.Checked = false;
                myCheckPic.Image = global::voteform.Properties.Resources._2;
            }
            else
            { myCheckPic.Checked = true;
                myCheckPic.Image = global::voteform.Properties.Resources._1;
            }
        }

        /// <summary>
        /// 增加控件视图
        /// </summary>
        public void addControlView()

        {
            //增加空间容器
            myPanel = new Panel();
            myPanel.Location = new System.Drawing.Point(30 + 130 * (index % Num_eachline), 3 + index / Num_eachline * 110);
            myPanel.Name = "panel_member" + index.ToString();
            myPanel.Size = new System.Drawing.Size(112, 110);
            myPanel.BorderStyle = BorderStyle.FixedSingle;
            
            //姓名
            myName = new TextBox();
            myName.AutoSize = true;
            myName.Location = new System.Drawing.Point(10, 5);
            myName.Name = "tbx_name_" + index.ToString();
            myName.Size = new System.Drawing.Size(90, 12);
            myName.TabIndex = 2 * index + 1;
            myName.Text = "填入姓名";
            myName.Font = new Font(myName.Font.FontFamily, 12, myName.Font.Style);
            //增加一个事件监听，点击即选中文字
            myName.Click += new System.EventHandler(this.SelectName);

            //投票勾选框
            //myCheck = new CheckBox();
            //myCheck.AutoSize = true;
            //myCheck.Location = new System.Drawing.Point(10, 45);
            //myCheck.Name = "cbx_vote_" + index.ToString();
            //myCheck.Size = new System.Drawing.Size(30, 30);
            //myCheck.TabIndex = 2 * index + 2;
            //myCheck.UseVisualStyleBackColor = true;

            //用图片控件代替投票勾选框
            myCheckPic = new CheckedPicbox();
            myCheckPic.Image = global::voteform.Properties.Resources._2;
            myCheckPic.Location = new System.Drawing.Point(30, 35);
            myCheckPic.Name = "pictureBox1";
            myCheckPic.Size = new System.Drawing.Size(50, 50);
            myCheckPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            myCheckPic.TabIndex = 2 * index + 2;
            myCheckPic.TabStop = false;
            myCheckPic.Click += new System.EventHandler(this.Checkpic);



            //结果
            myResult = new Label();
            myResult.AutoSize = true;
            myResult.Location = new System.Drawing.Point(45, 85);
            myResult.Name = "label1";
            myResult.Size = new System.Drawing.Size(62, 18);
            myResult.Font = new Font(myResult.Font.FontFamily, 12, myResult.Font.Style);
            myResult.Text = Num_result.ToString();

            ///将3个控件添加为penel的子控件
            myPanel.Controls.Add(myName);
            myPanel.Controls.Add(myCheckPic);
            myPanel.Controls.Add(myResult);


        }



    }
    public class CheckedPicbox: PictureBox
    {
        public bool Checked=false;

    }

}
